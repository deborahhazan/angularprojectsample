(function() {
    'use strict';

    angular.module('myLocations').run(CompanyRoot);

    CompanyRoot.$inject = ['validator', 'defaultErrorMessageResolver', 'ErrorService'];

    function CompanyRoot(validator, defaultErrorMessageResolver, ErrorService) {

        //validator message configuration
        validator.setValidElementStyling(false);
        defaultErrorMessageResolver.setI18nFileRootPath("languages-validate");
        defaultErrorMessageResolver.setCulture("en");
    }

})();