(function() {
    'use strict';

    angular.module('myLocations').constant('$settings', {
        version: "1.1.1",
        local_storage_prefix: "myLocations",
        service: 'http://localhost:3000'
    });
})();