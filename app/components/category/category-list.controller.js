(function() {
    angular.module('myLocations').controller('CategoryListController', CategoryListController);
    CategoryListController.$inject = ['$location', 'NgTableParams', 'CategoryService', 'CommonService'];

    function CategoryListController($location, NgTableParams, CategoryService, CommonService) {
        var vm = this;
        vm.selectedCategory = "";
        vm.remove = remove;
        vm.edit = edit;
        vm.add = add;
        vm.view = view;
        var data = CategoryService.getArray();
        //ng-table paramters definitions
        //set default rows in page to 5
        //allow 5,10,15,20 options in page to select
        vm.tableParams = new NgTableParams({
            count: 5
        }, { dataset: data, counts: [5, 10, 15, 20] });

        function remove() {
            //if selected category has assigned location we can't delete this category
            if (CategoryService.hasLocations(vm.selectedCategory))
                CommonService.showErrors(["This category has locations,delete the assigned locations before remove the category."]);
            else
                CommonService.showConfirm("Are you sure that you want to delete this category? </br> Warning: This action cannot be undone", "", function() {
                    var id = vm.selectedCategory;
                    CategoryService.remove(id);
                    //delete from ng-table module and from page
                    deleteFromTable(id);
                });
        }

        function deleteFromTable(id) {
            //this logic is needed to delete a row from table
            vm.tableParams.settings().dataset.removeByProperty("id", parseInt(id));
            vm.tableParams.reload().then(function(data) {
                if (data.length === 0 && vm.tableParams.total() > 0) {
                    vm.tableParams.page(vm.tableParams.page() - 1);
                    vm.tableParams.reload();
                }
            });
            vm.selectedCategory = "";
        }

        function edit() {
            var id = vm.selectedCategory;
            var url = '/';
            url += "category-edit?id=" + id
            $location.url(url);
        }

        function add() {
            var url = '/';
            url += "category-add"
            $location.url(url);
        }

        function view() {
            var id = vm.selectedCategory;
            var url = '/';
            url += "category-view?id=" + id
            $location.url(url);
        }
    }
})();