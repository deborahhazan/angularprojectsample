(function() {
    angular.module('myLocations').controller('CategoryViewController', CategoryViewController);
    CategoryViewController.$inject = ['CategoryService', '$routeParams'];

    function CategoryViewController(CategoryService, $routeParams) {
        var vm = this;
        vm.category = CategoryService.get($routeParams.id);
    }
})();