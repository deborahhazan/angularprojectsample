(function() {
    angular.module('myLocations').controller('CategoryAddController', CategoryAddController);
    CategoryAddController.$inject = ['$location', '$scope', 'CommonService', 'CategoryService'];

    function CategoryAddController($location, $scope, CommonService, CategoryService) {
        var vm = this;
        vm.submit = submit;

        function submit(category) {
            //validte category, need to check for duplicate name
            if (CategoryService.validate(category)) {
                CategoryService.add(category);
                CommonService.showMessages("Success!", ["Category is added successfully!"]);
                var url = "/category-list";
                $location.url(url);
            } else
                CommonService.showErrors(["Category with this name already exists!"]);
        }

    }
})();