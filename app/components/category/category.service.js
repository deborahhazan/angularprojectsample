(function() {
    'use strict';

    angular.module('myLocations').service('CategoryService', CategoryService);

    CategoryService.$inject = ['$filter', 'localStorageService'];

    function CategoryService($filter, localStorageService) {
        var s = this;
        s.add = add;
        s.remove = remove;
        s.getArray = getArray;
        s.get = get;
        s.edit = edit;
        s.validate = validate;
        s.hasLocations = hasLocations;

        //check if a specific category is assigned in some locations
        function hasLocations(category) {
            var obj = localStorageService.get("location-list");
            var found = false;
            angular.forEach(obj, function(value, key) {
                if (value.category && parseInt(category) == value.category.id)
                    found = true;
            });
            return found;
        }

        //validate category, check if have another category with the same name
        function validate(category) {
            var obj = getArray(localStorageService.get("category-list"));
            if (obj) {
                var foundCategory = $filter('filter')(obj, { name: category.name })[0];
                if (foundCategory && foundCategory.id != category.id)
                    return false;
            }

            return true;
        }

        //edit category in the local storage, edit inthe location list too
        function edit(category) {
            var obj = localStorageService.get("category-list");
            obj[category.id] = category;
            var locations = localStorageService.get("location-list");
            angular.forEach(locations, function(value, key) {
                if (value.category && category.id == value.category.id)
                    value.category.name = category.name;
            });
            localStorageService.set("location-list", locations);
            localStorageService.set("category-list", obj);
        }

        function get(id) {
            var obj = localStorageService.get("category-list");
            var category = obj[id];
            return category;
        }

        //list of categories is saved as object for direct get by key,so we need some conversion for ng-table or other
        function getArray() {
            var obj = localStorageService.get("category-list");
            var list = [];
            angular.forEach(obj, function(value, key) {
                list.push(value);
            });
            return list;
        }

        //add category
        function add(category) {
            var list = localStorageService.get("category-list");
            //counter for id 
            var counter = localStorageService.get("category-id-counter");
            if (counter)
                counter++;
            else
                counter = 1;
            localStorageService.set("category-id-counter", counter);
            category.id = counter;
            if (!list)
                list = {};
            list[category.id] = category;

            localStorageService.set("category-list", list);
        }

        function remove(id) {
            var list = localStorageService.get("category-list");
            if (list)
                delete list[id];
            localStorageService.set("category-list", list);
        }

    }
})();