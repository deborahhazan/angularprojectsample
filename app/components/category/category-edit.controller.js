(function() {
    angular.module('myLocations').controller('CategoryEditController', CategoryEditController);
    CategoryEditController.$inject = ['$location', 'CategoryService', '$routeParams', 'CommonService'];

    function CategoryEditController($location, CategoryService, $routeParams, CommonService) {
        var vm = this;
        vm.submit = submit;
        vm.category = CategoryService.get($routeParams.id);

        function submit(category) {
            //validate category for duplicate name
            if (CategoryService.validate(category)) {
                CategoryService.edit(category);
                CommonService.showMessages("Success!", ["Category is updated successfully!"]);
                var url = "/category-list";
                $location.url(url);
            } else
                CommonService.showErrors(["Category with this name already exists!"]);
        }

    }
})();