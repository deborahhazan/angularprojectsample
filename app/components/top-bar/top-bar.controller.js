(function() {
    angular.module('myLocations').controller('TopBarController', TopBarController);
    TopBarController.$inject = ['$location', '$http', '$settings'];

    function TopBarController($location, $http, $settings) {

        var vm = this
        vm.isActive = function(viewLocation) {
            return viewLocation === $location.path();
        };
        vm.connectToServer = function() {
            $http.get($settings.service + '/users/userlist').then(function(resp) {
                alert(JSON.stringify(resp));
                vm.success = true;
            }, function(error) {
                debugger;
                vm.error = true;
            });

        };
    }
})()