(function() {
    'use strict';

    angular.module('myLocations').service('LocationService', LocationService);

    LocationService.$inject = ['$filter', 'localStorageService'];

    function LocationService($filter, localStorageService) {
        var s = this;
        s.add = add;
        s.remove = remove;
        s.getArray = getArray;
        s.get = get;
        s.edit = edit;
        s.validate = validate;

        //validate for duplicate name
        function validate(location) {
            var obj = getArray(localStorageService.get("location-list"));
            if (obj) {
                var foundLocation = $filter('filter')(obj, { name: location.name })[0];
                if (foundLocation && foundLocation.id != location.id)
                    return false;
            }

            return true;
        }

        function edit(location) {
            var obj = localStorageService.get("location-list");
            obj[location.id] = location;
            localStorageService.set("location-list", obj);
        }

        function get(id) {
            var obj = localStorageService.get("location-list");
            var location = obj[id];
            return location;
        }
        //list of locations is saved as object for direct get by key,so we need some conversion for ng-table or other
        function getArray() {
            var obj = localStorageService.get("location-list");
            var list = [];
            angular.forEach(obj, function(value, key) {
                list.push(value);
            });
            return list;
        }

        function add(location) {
            var list = localStorageService.get("location-list");
            //id key counter
            var counter = localStorageService.get("location-id-counter");
            if (counter)
                counter++;
            else
                counter = 1;
            localStorageService.set("location-id-counter", counter);
            location.id = counter;
            if (!list)
                list = {};
            list[location.id] = location;

            localStorageService.set("location-list", list);
        }

        function remove(id) {
            var list = localStorageService.get("location-list");
            if (list)
                delete list[id];
            localStorageService.set("location-list", list);
        }

    }
})();