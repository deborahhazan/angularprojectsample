(function() {
    angular.module('myLocations').controller('LocationViewController', LocationViewController);
    LocationViewController.$inject = ['LocationService', '$routeParams'];

    function LocationViewController(LocationService, $routeParams) {
        var vm = this;
        vm.location = LocationService.get($routeParams.id);
    }
})();