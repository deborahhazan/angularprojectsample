(function() {
    angular.module('myLocations').controller('LocationListController', LocationListController);
    LocationListController.$inject = ['$location', 'NgTableParams', 'LocationService', 'CommonService'];

    function LocationListController($location, NgTableParams, LocationService, CommonService) {
        var vm = this;
        vm.selectedLocation = "";
        vm.remove = remove;
        vm.edit = edit;
        vm.add = add;
        vm.view = view;
        vm.map = map;
        vm.group = true;
        var data = LocationService.getArray();
        //ng-table paramters definitions
        //set default rows in page to 5
        //allow 5,10,15,20 options in page to select
        //this definition is for ungroup table
        vm.tableParams = new NgTableParams({
            count: 5
        }, { dataset: data, counts: [5, 10, 15, 20] });

        //ng-table paramters definitions
        //set default rows in page to 5
        //allow 5,10,15,20 options in page to select
        //this definition is for grouped by category table
        vm.tableParamsGrouped = new NgTableParams({
            count: 5,
            group: "category.name"
        }, { dataset: data, counts: [5, 10, 15, 20] });

        function remove() {
            CommonService.showConfirm("Are you sure that you want to delete this location? </br> Warning: This action cannot be undone", "", function() {
                var id = vm.selectedLocation;
                LocationService.remove(id);
                deleteFromTable(id);
            });
        }



        function deleteFromTable(id) {
            vm.tableParams.settings().dataset.removeByProperty("id", parseInt(id));
            vm.tableParams.reload().then(function(data) {
                if (data.length === 0 && vm.tableParams.total() > 0) {
                    vm.tableParams.page(vm.tableParams.page() - 1);
                    vm.tableParams.reload();
                }
            });
            vm.tableParamsGrouped.settings().dataset.removeByProperty("id", parseInt(id));
            vm.tableParamsGrouped.reload().then(function(data) {
                if (data.length === 0 && vm.tableParamsGrouped.total() > 0) {
                    vm.tableParamsGrouped.page(vm.tableParamsGrouped.page() - 1);
                    vm.tableParamsGrouped.reload();
                }
            });
            vm.selectedLocation = "";
        }

        function edit() {
            var id = vm.selectedLocation;
            var url = '/';
            url += "location-edit?id=" + id
            $location.url(url);
        }

        function add() {
            var url = '/';
            url += "location-add"
            $location.url(url);
        }

        function view() {
            var id = vm.selectedLocation;
            var url = '/';
            url += "location-view?id=" + id
            $location.url(url);
        }

        function map() {
            var id = vm.selectedLocation;
            var url = '/';
            url += "location-view-map?id=" + id
            $location.url(url);
        }
    }
})();