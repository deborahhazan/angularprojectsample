(function() {
    angular.module('myLocations').controller('LocationEditController', LocationEditController);
    LocationEditController.$inject = ['CategoryService', '$location', 'LocationService', '$routeParams', 'CommonService'];

    function LocationEditController(CategoryService, $location, LocationService, $routeParams, CommonService) {
        var vm = this;
        vm.submit = submit;
        vm.location = LocationService.get($routeParams.id);
        vm.categories = CategoryService.getArray();
        vm.dropDownconfig = {
            modelLabel: "name",
            optionLabel: "name"
        };
        vm.errors = { list: [] };

        function submit(location) {
            //check for address errors
            if (vm.errors.list && vm.errors.list.length > 0) {
                CommonService.showErrors(vm.errors.list);
                return false;
            }
            //validate for duplicate name
            if (LocationService.validate(location)) {
                LocationService.edit(location);
                CommonService.showMessages("Success!", ["Location is updated successfully!"]);
                var url = "/location-list";
                $location.url(url);
            } else
                CommonService.showErrors(["Location with this name already exists!"]);
        }

    }
})();