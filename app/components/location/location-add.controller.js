(function() {
    angular.module('myLocations').controller('LocationAddController', LocationAddController);
    LocationAddController.$inject = ['CategoryService', '$location', '$scope', 'CommonService', 'LocationService'];

    function LocationAddController(CategoryService, $location, $scope, CommonService, LocationService) {
        var vm = this;
        vm.submit = submit;
        vm.categories = CategoryService.getArray();
        vm.dropDownconfig = {
            modelLabel: "name",
            optionLabel: "name"
        };
        vm.location = {

        };
        vm.location.address = {
            city: "",
            street: "",
            number: "",
            country: "",
            address: ""
        };
        vm.location.coordinates = {
            latitude: 17,
            longitude: 19
        };
        //initialize error object for address directive
        vm.errors = { list: [] };

        function submit(location) {
            //check for errors in address
            if (vm.errors.list && vm.errors.list.length > 0) {
                CommonService.showErrors(vm.errors.list);
                return false;
            }
            //check for duplicate name
            if (LocationService.validate(location)) {
                LocationService.add(location);
                CommonService.showMessages("Success!", ["Location is added successfully!"]);
                var url = "/location-list";
                $location.url(url);
            } else
                CommonService.showErrors(["Location with this name already exists!"]);
        }
    }
})();