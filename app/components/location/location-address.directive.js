(function() {
    'use strict';

    angular.module('myLocations').directive('mylocationsLocationAddress', mylocationsLocationAddressDirective);

    mylocationsLocationAddressDirective.$inject = [];

    function mylocationsLocationAddressDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/location/location-address.html',
            controller: 'LocationAddressController',
            controllerAs: 'locationaddress',
            scope: {
                address: "=",
                coordinates: "=",
                errors: "="

            }
        };
        return directive;
    }
})();