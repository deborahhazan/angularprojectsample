(function() {
    angular.module('myLocations').controller('LocationViewMapController', LocationViewMapController);
    LocationViewMapController.$inject = ['LocationService', '$routeParams'];

    function LocationViewMapController(LocationService, $routeParams) {
        var vm = this;
        vm.location = LocationService.get($routeParams.id);
        vm.map = {
            options: function() {
                return {
                    streetViewControl: false,
                    zoom: 15,
                    mapTypeControl: true,
                    mapTypeId: google.maps.MapTypeId.HYBRID,
                }
            },
            center: [vm.location.coordinates.latitude, vm.location.coordinates.longitude],
            marker: {
                position: [vm.location.coordinates.latitude, vm.location.coordinates.longitude]
            }
        }
    }
})();