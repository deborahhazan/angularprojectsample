(function() {
    'use strict';

    angular.module('myLocations').controller('LocationAddressController', LocationAddressController);

    LocationAddressController.$inject = ['Geocoder', '$scope'];

    function LocationAddressController(Geocoder, $scope) {
        var vm = this;
        //get data from directive scope
        vm.model = $scope.address;
        vm.coordinates = $scope.coordinates;
        vm.closeAlert = closeAlert;
        vm.applyAdress = applyAdress;
        vm.updateLocation = updateLocation;
        vm.onChange = onChange;
        vm.errors = $scope.errors;
        //initialize map and marker definition
        vm.map = {
            control: {},
            center: [vm.coordinates.latitude, vm.coordinates.longitude],
            zoom: 15,
            bounds: null,
            options: function() {
                return {
                    mapTypeId: google.maps.MapTypeId.HYBRID,
                    streetViewControl: false,
                    mapTypeControl: true,
                    zoomControl: true,
                    panControl: false,
                    draggable: true,
                    disableDoubleClickZoom: false,
                    scrollwheel: false,
                    zoom: 15
                }
            },
            googlePoint: {
                id: -1,
                address: "",
                location: null,
                events: {
                    dragend: function(ele) {
                        getAddress(ele);
                    }
                },
                position: [vm.coordinates.latitude, vm.coordinates.longitude],
                options: function() {
                    return {
                        draggable: true,
                        title: "Address Point"
                    }
                }
            }
        };

        //on address input change add error for check location before submit
        function onChange() {
            vm.errors.list = ["Please update location before submit"];
        }

        function closeAlert(index) {
            vm.locationMsg.splice(index, 1);
        }

        //get address by latlng, when user move the pushpin in the map update location
        function getAddress(ele) {
            Geocoder.reverseLookup({
                'latlng': ele.latLng.toUrlValue()
            }).then(function(result) {
                if (result.status == google.maps.GeocoderStatus.OK) {
                    var location = result.results[0];
                    if (location) {
                        vm.map.googlePoint.address = location.formatted_address;
                        if (location.types[0] == 'street_address') {
                            vm.showAdressPopup = true;
                            vm.locationMsg = [];
                            vm.map.googlePoint.location = location;
                        } else {
                            vm.showAdressPopup = false;
                            vm.locationMsg = [{
                                type: "danger",
                                msg: "Address is not valid"
                            }];
                        }
                    }
                }
            });
        }

        //apply address after move pushpin marker on the map
        function applyAdress() {
            var location = vm.map.googlePoint.location;
            vm.model.address = location.formatted_address;
            vm.coordinates.latitude = location.geometry.location.lat;
            vm.coordinates.longitude = location.geometry.location.lng;
            angular.forEach(location.address_components, function(value, key) {
                if (value.types[0] == 'street_number') {
                    vm.model.number = value.long_name;
                }
                if (value.types[0] == 'route') {
                    vm.model.street = value.long_name;
                }
                if (value.types[0] == 'locality') {
                    vm.model.city = value.long_name;
                }
                if (value.types[0] == 'postal_code') {
                    vm.model.zip = value.long_name;
                }
                if (value.types[0] == 'country') {
                    vm.model.country = value.long_name;
                }
            });
            vm.errors.list = [];
            vm.showAdressPopup = false;
            vm.locationMsg = [];
        };


        //update location on the map by the fields address value
        function updateLocation() {
            vm.showAdressPopup = false;
            vm.locationMsg = [];
            var address = '';
            if (vm.model.country !== null && vm.model.country !== undefined) {
                address += vm.model.country + ', ';
            }
            if (vm.model.street !== null && vm.model.street !== undefined) {
                address += vm.model.street + ', ';
            }
            if (vm.model.number !== null && vm.model.number !== undefined) {
                address += vm.model.number + ', ';
            }
            if (vm.model.city !== null && vm.model.city !== undefined) {
                address += vm.model.city + ', ';
            }
            if (vm.model.zip !== null && vm.model.zip !== undefined) {
                address += vm.model.zip + ', ';
            }
            if (vm.model.country !== null && vm.model.country !== undefined) {
                address += vm.model.country + ', ';
            }
            //get gelocation from google by address
            Geocoder.reverseLookup({
                'address': address
            }).then(function(result) {
                //if we have some location but not an exact address location show on the map
                //but not select this location
                if (result.status == google.maps.GeocoderStatus.OK) {
                    var res = result.results[0];
                    vm.map.center = [res.geometry.location.lat, res.geometry.location.lng];
                    vm.map.googlePoint.position = [res.geometry.location.lat, res.geometry.location.lng];
                }
                //if we have an exact address, this page allow only exact addess
                if (result.status == google.maps.GeocoderStatus.OK && result.results && result.results.length > 0 && result.results[0].types.indexOf("street_address") > -1) {
                    vm.locationMsg = [];
                    var res = result.results[0];
                    vm.errors.list = [];
                    vm.model.address = res.formatted_address;
                    vm.coordinates.latitude = res.geometry.location.lat;
                    vm.coordinates.longitude = res.geometry.location.lng;
                    vm.locationMsg = [{
                        msg: 'This address is valid!',
                        type: 'success'
                    }];
                } else {
                    vm.errors.list = ['Not valid address'];
                    vm.locationMsg = [{
                        msg: 'Not valid addresss',
                        type: 'danger'
                    }];
                }
            });
        }
    }
})();