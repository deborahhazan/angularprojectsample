(function() {
    'use strict';
    angular.module('myLocations').factory('ErrorInterceptorFactory', ErrorInterceptorFactory);
    ErrorInterceptorFactory.$inject = ['$rootScope', '$q', 'ErrorConstant'];

    function ErrorInterceptorFactory($rootScope, $q, ErrorConstant) {
        return {
            responseError: function(response) {
                //broadcast appropiate event message if the server request failed
                var broadcastEvent = ErrorConstant.events.unknown;
                if (response.status == 500)
                    broadcastEvent = ErrorConstant.events.internalServerError;
                else if (response.status == 401 || response.status == 403 || response.status == 419 || response.status == 440)
                    broadcastEvent = "";
                else if (response.status == 0 || response.status == -1)
                    broadcastEvent = "";
                else if (response.status >= 400 && response.status < 500)
                    broadcastEvent = ErrorConstant.events.clientError;
                else if (response.status > 500 && response.status < 600)
                    broadcastEvent = ErrorConstant.events.serverError;

                if(broadcastEvent != "")
                  $rootScope.$broadcast(broadcastEvent, response);

                return $q.reject(response);
            }
        };
    }
})();