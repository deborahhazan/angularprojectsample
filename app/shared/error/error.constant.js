(function() {
    'use strict';

    var ErrorConstant = {
        events: {
            internalServerError: 'internal-server-error',
            serverError: 'server-error',
            clientError: 'client-error',
            unknown: "unknown",
            googleError: 'google-error'
        }
    }

    angular.module('myLocations').constant('ErrorConstant', ErrorConstant);
})();