(function() {
    'use strict';

    angular.module('myLocations').service('ErrorService', ErrorService);

    ErrorService.$inject = ['$rootScope', 'ErrorConstant', 'CommonService'];

    function ErrorService($rootScope, ErrorConstant, CommonService) {
        var vm = this;
        //show appropiate error message 
        $rootScope.$on(ErrorConstant.events.internalServerError, function(event, resp) {

            var error = "internalServerError";
            if (resp.data && resp.data.error) {
                error = resp.data.error;
            }
            CommonService.showErrors([error]);
        });
        $rootScope.$on(ErrorConstant.events.serverError, function(resp) {
            CommonService.showErrors(["serverError"]);
        });
        $rootScope.$on(ErrorConstant.events.unknown, function(resp) {
            CommonService.showErrors(["unknown"]);
        });
        $rootScope.$on(ErrorConstant.events.clientError, function(resp) {
            CommonService.showErrors(["clientError"]);
        });
        $rootScope.$on(ErrorConstant.events.googleError, function(event, resp) {
            var error = "google error";
            if (resp.status) {
                error = resp.status;
            }
            CommonService.showErrors([error]);
        });
    }
})();