(function() {
    'use strict';
    angular.module('myLocations').factory('ParamsInterceptorFactory', ParamsInterceptorFactory);

    ParamsInterceptorFactory.$inject = ['$rootScope', '$q', '$location', '$settings', 'URI', '$templateCache']

    function ParamsInterceptorFactory($rootScope, $q, $location, $settings, URI, $templateCache) {
        return {
            request: function(config) {
                //add version to url for prevent cache on change version
                if ($templateCache.get(config.url))
                    return config;
                config.url = URI(config.url).addSearch({
                    'version': $settings.version
                }).toString();
                return config;
            }
        };
    }
})();