(function() {
    'use strict';

    angular.module('myLocations').service('CommonService', CommonService);

    CommonService.$inject = ['$modal', '$window'];

    function CommonService($modal, $window) {
        var s = this;
        s.showMessages = showMessages;
        s.showConfirm = showConfirm;
        s.showErrors = showErrors;
        s.setTitle = setTitle;
        s.IEVersion = detectIE;
        s.removeParam = removeParam;

        function setTitle(title) {
            $window.document.title = title;
        }

        function showErrors(errors) {
            $modal.open({
                templateUrl: 'app/shared/modal/modal-error.html',
                controller: 'ModalErrorController',
                controllerAs: 'modal',
                size: "md",
                backdrop: true,
                resolve: {
                    errors: function() {
                        return errors;
                    }
                }
            });
        }

        function showConfirm(message, cssClass, ok, cancel) {
            var modalInstance = $modal.open({
                templateUrl: 'app/shared/modal/modal-confirm.html',
                controller: 'ModalConfirmController',
                controllerAs: 'modal',
                backdrop: false,
                windowClass: cssClass + " modal-confirm",
                size: "sm",
                resolve: {
                    message: function() {
                        return message;
                    }
                }
            });

            modalInstance.result.then(ok, cancel);
        }

        function showMessages(title, messages) {
            $modal.open({
                templateUrl: 'app/shared/modal/modal-message.html',
                controller: 'ModalMessageController',
                controllerAs: 'modal',
                backdrop: true,
                resolve: {
                    title: function() {
                        return title;
                    },
                    messages: function() {
                        return messages;
                    }
                }
            });
        }

        /**
         * detect IE
         * returns version of IE or false, if browser is not Internet Explorer
         */
        function detectIE() {
            var ua = window.navigator.userAgent;

            // test values
            // IE 10
            //ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
            // IE 11
            //ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
            // IE 12 / Spartan
            //ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

            var msie = ua.indexOf('MSIE ');
            if (msie > 0) {
                // IE 10 or older => return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }

            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                // IE 11 => return version number
                var rv = ua.indexOf('rv:');
                return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }

            var edge = ua.indexOf('Edge/');
            if (edge > 0) {
                // IE 12 => return version number
                return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            }

            // other browser
            return false;
        }

        /** Remove a param from a query string
         *
         * @param queryString - initial query string
         * @param paramName - param to remove
         * @returns {*} - a new query string
         */
        function removeParam(queryString, paramName) {
            var index_start = queryString.indexOf(paramName + "=");
            var tmp_truncated = queryString.substr(index_start);
            var index_stop = tmp_truncated.indexOf("&");
            tmp_truncated = tmp_truncated.substr(0, index_stop > -1 ? index_stop : tmp_truncated.length);
            var new_queryString = queryString.replace("&" + tmp_truncated, "");
            return new_queryString;
        }

    }
})();