(function() {
    'use strict';

    angular.module('myLocations').controller('ModalMessageController', ModalMessageController);

    ModalMessageController.$inject = ['$location', '$modalInstance', 'title', 'messages'];

    function ModalMessageController($location, $modalInstance, title, messages) {
        var vm = this;
        vm.title = title;
        vm.messages = messages;
        vm.close = close;

        function close() {

            $modalInstance.dismiss('cancel');
            if (vm.title == 'autoModal.resetting.checkEmail.title' || vm.title == 'autoModal.register.checkEmail.title' || vm.title == 'autoModal.deleteUser.userDeleted.title') {
                $location.url("/home");

            }
        }
    }
})();