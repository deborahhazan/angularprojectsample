(function() {
    'use strict';

    angular.module('myLocations').controller('ModalErrorController', ModalErrorController);

    ModalErrorController.$inject = ['$modalInstance', 'errors'];

    function ModalErrorController($modalInstance, errors) {
        var vm = this;
        vm.errors = errors;
        vm.close = close;

        function close() {
            $modalInstance.dismiss('cancel');
        }
    }
})();