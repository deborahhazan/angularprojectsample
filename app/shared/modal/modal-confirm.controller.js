(function() {
    'use strict';

    angular.module('myLocations').controller('ModalConfirmController', ModalConfirmController);

    ModalConfirmController.$inject = ['$modalInstance', 'message'];

    function ModalConfirmController($modalInstance, message) {
        var vm = this;
        vm.message = message;
        vm.ok = ok;
        vm.cancel = cancel;

        function ok() {
            $modalInstance.close();
        };

        function cancel() {
            $modalInstance.dismiss('cancel');
        };
    }
})();