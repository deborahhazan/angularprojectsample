(function() {
    'use strict';

    angular.module('myLocations').config(CompanyConfig);

    CompanyConfig.$inject = ['$settings', '$httpProvider', 'localStorageServiceProvider'];

    function CompanyConfig($settings, $httpProvider, localStorageServiceProvider) {

        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common["X-Requested-With"];

        //Add error interceptor on $http request end, for handle $http error 
        $httpProvider.interceptors.push([
            '$injector',
            function($injector) {
                return $injector.get('ErrorInterceptorFactory');
            }
        ]);

        //Add interceptor for add some params to url
        $httpProvider.interceptors.push([
            '$injector',
            function($injector) {
                return $injector.get('ParamsInterceptorFactory');
            }
        ]);

        localStorageServiceProvider.setPrefix($settings.local_storage_prefix).setStorageType('localStorage');
    }
})();