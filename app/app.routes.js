(function() {
    'use strict';

    angular.module('myLocations').config(RouteConfig);
    RouteConfig.$inject = ['$routeProvider'];


    function RouteConfig($routeProvider) {
        $routeProvider.
        when('/', {
            templateUrl: 'app/components/home/home.html'
        }).
        when('/home', {
            templateUrl: 'app/components/home/home.html'
        }).
        when('/category-list', {
            templateUrl: 'app/components/category/category-list.html',
            controller: 'CategoryListController',
            controllerAs: "list"
        }).
        when('/category-add', {
            templateUrl: 'app/components/category/category-add.html',
            controller: 'CategoryAddController',
            controllerAs: "add"
        }).when('/category-view', {
            templateUrl: 'app/components/category/category-view.html',
            controller: 'CategoryViewController',
            controllerAs: "view"
        }).when('/category-edit', {
            templateUrl: 'app/components/category/category-edit.html',
            controller: 'CategoryEditController',
            controllerAs: "edit"
        }).
        when('/location-list', {
            templateUrl: 'app/components/location/location-list.html',
            controller: 'LocationListController',
            controllerAs: "list"
        }).
        when('/location-add', {
            templateUrl: 'app/components/location/location-add.html',
            controller: 'LocationAddController',
            controllerAs: "add"
        }).when('/location-view', {
            templateUrl: 'app/components/location/location-view.html',
            controller: 'LocationViewController',
            controllerAs: "view"
        }).when('/location-edit', {
            templateUrl: 'app/components/location/location-edit.html',
            controller: 'LocationEditController',
            controllerAs: "edit"
        }).when('/location-view-map', {
            templateUrl: 'app/components/location/location-view-map.html',
            controller: 'LocationViewMapController',
            controllerAs: "map"
        })
    }
})();