(function() {
    'use strict';
    //	declare new module for company app, and add all the other modules used in the app
    angular.module('myLocations', [
        'ui.bootstrap',
        'ngRoute',
        'jcs-autoValidate',
        'angular-uri',
        'LocalStorageModule',
        'ngTable',
        'apg.typeaheadDropdown',
        'ngMaps',
        'geocoder',
        'ngTouch'
    ]);
})();